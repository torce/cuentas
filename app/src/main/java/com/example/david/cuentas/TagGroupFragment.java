package com.example.david.cuentas;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.david.cuentas.util.CursorRecyclerViewAdapter;

import java.text.NumberFormat;

public class TagGroupFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	static final int TAG_GROUPS_LOADER = 0;

	// Custom column naming
	private static final String INCOMES_COL = "incomes";
	private static final String EXPENSES_COL = "expenses";
	private static final String TOTAL_COL = "total";

	public interface TagGroupClickListener {
		public void onTagGroupClick(String tag);
	}

	private TagGroupCursorAdapter mAdapter;
	private TagGroupClickListener mListener;

	public static TagGroupFragment newInstance() {
		return new TagGroupFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_tag_group, container, false);

		RecyclerView mTagGroupView = (RecyclerView) view.findViewById(R.id.recycler_view);
		mTagGroupView.setHasFixedSize(true);
		LinearLayoutManager manager = new LinearLayoutManager(getActivity());
		manager.setOrientation(LinearLayoutManager.VERTICAL);
		mTagGroupView.setLayoutManager(manager);

		mAdapter = new TagGroupCursorAdapter(null);
		mTagGroupView.setAdapter(mAdapter);

		getLoaderManager().initLoader(TAG_GROUPS_LOADER, null, this);
		return view;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id) {
			case TAG_GROUPS_LOADER:
				Uri queryUri = OperationsProvider.OPERATIONS_URI.buildUpon()
								.appendQueryParameter("groupBy", OperationsProvider.OP_TAG).build();
				final String amount = OperationsProvider.OP_AMOUNT;
				return new CursorLoader(getActivity(),
								queryUri,
								new String[]{"min(" + OperationsProvider.OP_ID + ") AS " + OperationsProvider.OP_ID,
												OperationsProvider.OP_TAG,
												"sum(case when " + amount + " > 0 THEN " + amount + " ELSE 0 END) AS " + INCOMES_COL,
												"sum(case when " + amount + " < 0 THEN " + amount + " ELSE 0 END) AS " + EXPENSES_COL,
												"sum(" + amount + ") AS " + TOTAL_COL},
								null,
								null,
								null);
			default:
				throw new IllegalArgumentException("Unknown loader id: " + id);
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		switch (loader.getId()) {
			case TAG_GROUPS_LOADER:
				mAdapter.changeCursor(data);
				break;
			default:
				throw new IllegalArgumentException("Unknown loader id: " + loader.getId());
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		onLoadFinished(loader, null);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mListener = (TagGroupClickListener) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	class TagGroupViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

		TextView mTagGroup;
		TextView mIncomes;
		TextView mExpenses;
		TextView mTotal;

		public TagGroupViewHolder(View v) {
			super(v);
			mTagGroup = (TextView) v.findViewById(R.id.group_tag);
			mIncomes = (TextView) v.findViewById(R.id.incomes);
			mExpenses = (TextView) v.findViewById(R.id.expenses);
			mTotal = (TextView) v.findViewById(R.id.total);
		}

		@Override
		public void onClick(View v) {
			mListener.onTagGroupClick(mTagGroup.getText().toString());
		}
	}

	class TagGroupCursorAdapter extends CursorRecyclerViewAdapter<TagGroupViewHolder> {

		public TagGroupCursorAdapter(Cursor cursor) {
			super(cursor);
		}

		@Override
		public void onBindViewHolderCursor(TagGroupViewHolder holder, Cursor data) {
			holder.mTagGroup.setText(data.getString(data.getColumnIndex(OperationsProvider.OP_TAG)));

			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			Double incomes = data.getDouble(data.getColumnIndex(INCOMES_COL));
			if (incomes != .0) {
				holder.mIncomes.setText(formatter.format(incomes));
			}

			Double expenses = data.getDouble(data.getColumnIndex(EXPENSES_COL));
			if (expenses != .0) {
				holder.mExpenses.setText(formatter.format(expenses));
			}

			holder.mTotal.setText(formatter.format(data.getDouble(data.getColumnIndex(TOTAL_COL))));
		}

		@Override
		public TagGroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View tagGroupView = LayoutInflater
							.from(parent.getContext())
							.inflate(R.layout.tag_group_view, parent, false);
			TagGroupViewHolder holder = new TagGroupViewHolder(tagGroupView);
			tagGroupView.setOnClickListener(holder);
			return holder;
		}
	}
}
