package com.example.david.cuentas;

import android.app.Activity;
import android.os.Bundle;


public class DetailsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);

		setTitle(getString(R.string.title_activity_details));
		
		Bundle args = getIntent().getExtras();
		String tag = args.getString(DetailsFragment.TAG_KEY);
		String date = args.getString(DetailsFragment.DATE_KEY);
		int operationType = args.getInt(DetailsFragment.OPERATION_TYPE_KEY, -1);

		DetailsFragment fragment;
		if (tag != null) {
			fragment = DetailsFragment.newInstance(tag);
		} else if (date != null && operationType >= 0) {
			fragment = DetailsFragment.newInstance(date, operationType);
		} else {
			throw new IllegalArgumentException("Invalid arguments for DetailsActivity");
		}

		getFragmentManager().beginTransaction().replace(R.id.fragment, fragment).commit();
	}

}
