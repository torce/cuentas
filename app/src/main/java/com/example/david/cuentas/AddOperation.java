package com.example.david.cuentas;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.google.android.gms.common.api.GoogleApiClient.Builder;
import static com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import static com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;


public class AddOperation extends Activity
				implements LoaderManager.LoaderCallbacks<Cursor>, ConnectionCallbacks, OnConnectionFailedListener {

	public static final String OPERATION_KEY = "OPERATION";
	public static final int OPERATION_EXPENSE = 0;
	public static final int OPERATION_INCOME = 1;

	public static final String RESULT_KEY = "RESULT";
	static final int TAGS_LOADER = 0;

	private static final int REQUEST_PHOTO = 1;

	private int mOperationType;

	public static final String THUMBNAIL_SUFIX = "_thumb";
	private static final int THUMBNAIL_SIZE = 200;


	private TagCursorAdapter mAdapter;
	private GoogleApiClient mGoogleApiClient;

	private EditText mAmount;
	private EditText mTag;
	private EditText mDesc;
	private ImageView mImage;
	private File mPhoto;
	private Location mLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_operation);

		mOperationType = getIntent().getIntExtra(OPERATION_KEY, -1);
		if (mOperationType < 0) {
			throw new IllegalArgumentException("Invalid operation code: " + mOperationType);
		}

		if (mOperationType == OPERATION_EXPENSE) {
			setTitle(getString(R.string.title_activity_add_expense));
		} else {
			setTitle(getString(R.string.title_activity_add_income));
		}

		mAmount = (EditText) findViewById(R.id.i_amount);
		mTag = (EditText) findViewById(R.id.i_tag);
		mDesc = (EditText) findViewById(R.id.i_desc);
		mImage = (ImageView) findViewById(R.id.imageView);

		mAdapter = new TagCursorAdapter(this, null, false);
		GridView tagView = (GridView) findViewById(R.id.tag_view);
		tagView.setAdapter(mAdapter);
		tagView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView parent, View view, int position, long id) {
				String text = ((TagViewHolder) view.getTag()).mTagName.getText().toString();
				mTag.setText(text);
			}
		});

		getLoaderManager().initLoader(TAGS_LOADER, null, this);

		buildGoogleApiClient();
		mGoogleApiClient.connect();

		findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Operation result = validate();

				if (result != null) {
					Intent returnIntent = getIntent();
					returnIntent.putExtra(RESULT_KEY, Parcels.wrap(result));
					setResult(RESULT_OK, returnIntent);
					finish();
				}
			}
		});

		findViewById(R.id.photo).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				if (takePhoto.resolveActivity(getPackageManager()) != null) {
					try {
						mPhoto = createImageFile();
					} catch (IOException ex) {
						return;
					}
					if (mPhoto != null) {
						takePhoto.putExtra(MediaStore.EXTRA_OUTPUT,
										Uri.fromFile(mPhoto));
						startActivityForResult(takePhoto, REQUEST_PHOTO);
					}
				}
			}
		});
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return File.createTempFile(
                    imageFileName,  // prefix
                    ".jpg",         // suffix
                    storageDir      // directory
            );
        } else {
            throw new IOException("External storage not ready.");
        }
	}

	private Operation validate() {
		boolean validation = true;

		Double amount = .0;
		try {
			amount = Double.valueOf(mAmount.getText().toString());
			if (amount.equals(.0)) {
				mAmount.setError("Amount cannot be 0.");
				validation = false;
			}
		} catch (NumberFormatException e) {
			mAmount.setError("Amount cannot be empty.");
			validation = false;
		}
		if (amount < 0) {
			mAmount.setError("Amount cannot be negative.");
			validation = false;
		}

		String tag = mTag.getText().toString().trim();
		if (tag.isEmpty()) {
			mTag.setError("A tag value is required.");
			validation = false;
		}
		if (tag.length() > 20) {
			mTag.setError("Tag cannot be larger than 20 characters.");
			validation = false;
		}

		String description = mDesc.getText().toString().trim();
		if (description.length() > 144) {
			mDesc.setError("Description should be smaller than 144 characters.");
			validation = false;
		}

		String photoPath = null;
		if (mPhoto != null) {
			photoPath = mPhoto.getAbsolutePath();
			// Get the filename without extension
			int pos = photoPath.lastIndexOf('.');
			if (pos > 0) {
				photoPath = photoPath.substring(0, pos);
			}
		}

		Double latitude = null;
		Double longitude = null;
		if (mLocation != null) {
			latitude = mLocation.getLatitude();
			longitude = mLocation.getLongitude();
		}
		if (validation) {
			return new Operation(amount, tag, description, new Date(), photoPath, latitude, longitude);
		} else {
			return null;
		}
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new Builder(this)
						.addConnectionCallbacks(this)
						.addOnConnectionFailedListener(this)
						.addApi(LocationServices.API)
						.build();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_PHOTO && resultCode == RESULT_OK) {
			Bitmap thumbNail = ThumbnailUtils.extractThumbnail(
							BitmapFactory.decodeFile(
											mPhoto.getAbsolutePath()),
							THUMBNAIL_SIZE, THUMBNAIL_SIZE);
			try {
				// Get the filename without extension
				String thumbNailName = mPhoto.getAbsolutePath();
				int pos = thumbNailName.lastIndexOf('.');
				if (pos > 0) {
					thumbNailName = thumbNailName.substring(0, pos) + THUMBNAIL_SUFIX + ".jpg";
				}

				FileOutputStream thumbNailFile = new FileOutputStream(new File(thumbNailName));
				mImage.setImageBitmap(thumbNail);
				thumbNail.compress(Bitmap.CompressFormat.JPEG, 100, thumbNailFile);


				Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				Uri contentUri = Uri.fromFile(mPhoto);
				mediaScanIntent.setData(contentUri);
				this.sendBroadcast(mediaScanIntent);
			} catch (FileNotFoundException e) {
				Log.e(AddOperation.class.getCanonicalName(), e.toString());
			}

		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id) {
			case TAGS_LOADER:
				Uri queryUri = OperationsProvider.TAGS_URI.buildUpon()
								.appendQueryParameter("limit", "6").build();
				String where;
				if (mOperationType == OPERATION_EXPENSE) {
					where = OperationsProvider.OP_AMOUNT + " <= 0";
				} else {
					where = OperationsProvider.OP_AMOUNT + " > 0";
				}
				return new CursorLoader(this, queryUri,
								new String[]{OperationsProvider.OP_ID,
												OperationsProvider.OP_TAG},
								where, null, "_count DESC");
			default:
				throw new IllegalArgumentException("Unknown loader id: " + id);
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		switch (loader.getId()) {
			case TAGS_LOADER:
				mAdapter.changeCursor(data);
				break;
			default:
				throw new IllegalArgumentException("Unknown loader id: " + loader.getId());
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		onLoadFinished(loader, null);
	}

	@Override
	public void onConnected(Bundle bundle) {
		mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}

	class TagCursorAdapter extends CursorAdapter {

		public TagCursorAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view = LayoutInflater
							.from(context)
							.inflate(R.layout.tag_view, parent, false);
			view.setTag(new TagViewHolder(view));
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			TagViewHolder holder = (TagViewHolder) view.getTag();
			holder.mTagName.setText(cursor.getString(cursor.getColumnIndex(OperationsProvider.OP_TAG)));
		}
	}

	class TagViewHolder {

		protected TextView mTagName;

		public TagViewHolder(View v) {
			mTagName = (TextView) v.findViewById(R.id.tag_name);
		}
	}
}
