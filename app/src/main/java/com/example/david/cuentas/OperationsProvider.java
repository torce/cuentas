package com.example.david.cuentas;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class OperationsProvider extends ContentProvider {
	public static final String PROVIDER_NAME = "com.example.david.cuentas.OperationsProvider";

	public static final String OPERATIONS_URL = "content://" + PROVIDER_NAME + "/operations";
	public static final Uri OPERATIONS_URI = Uri.parse(OPERATIONS_URL);

	public static final String TAGS_URL = "content://" + PROVIDER_NAME + "/tags";
	public static final Uri TAGS_URI = Uri.parse(TAGS_URL);

	// Operations grouped by day
	public static final String DAILY_URL = "content://" + PROVIDER_NAME + "/operations/daily";
	public static final Uri DAILY_URI = Uri.parse(DAILY_URL);

	// Operation columns
	public static final String OP_ID = "_id";
	public static final String OP_AMOUNT = "amount";
	public static final String OP_TAG = "tag";
	public static final String OP_DESCRIPTION = "description";
	public static final String OP_DATE = "date";
	public static final String OP_PHOTO = "photo";
	public static final String OP_LATITUDE = "latitude";
	public static final String OP_LONGITUDE = "longitude";

	//Integer values for UriMatcher clauses
	static final int OPERATIONS = 1;
	static final int OPERATIONS_ID = 2;
	static final int TAGS = 3;
	static final int DAILY = 4;

	DBHelper dbHelper;

	private static Map<String, String> operationsMap;

	static {
		operationsMap = new HashMap<>();
		operationsMap.put(OP_ID, OP_ID);
		operationsMap.put(OP_AMOUNT, OP_AMOUNT);
		operationsMap.put(OP_TAG, OP_TAG);
		operationsMap.put(OP_DESCRIPTION, OP_DESCRIPTION);
		operationsMap.put(OP_DATE, OP_DATE);
		operationsMap.put(OP_PHOTO, OP_PHOTO);
		operationsMap.put(OP_LATITUDE, OP_LATITUDE);
		operationsMap.put(OP_LONGITUDE, OP_LONGITUDE);
	}

	static final UriMatcher uriMatcher;

	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(PROVIDER_NAME, "operations", OPERATIONS);
		uriMatcher.addURI(PROVIDER_NAME, "operations/#", OPERATIONS_ID);
		uriMatcher.addURI(PROVIDER_NAME, "tags", TAGS);
		uriMatcher.addURI(PROVIDER_NAME, "operations/daily", DAILY);
	}

	private SQLiteDatabase database;
	static final String DATABASE_NAME = "operationsDB";
	static final int DATABASE_VERSION = 1;

	static final String OPERATIONS_TABLE_NAME = "operations";

	static final String CREATE_OPERATIONS_TABLE =
					"CREATE TABLE " + OPERATIONS_TABLE_NAME +
									"(" + OP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
									OP_AMOUNT + " REAL NOT NULL," +
									OP_TAG + " TEXT NOT NULL," +
									OP_DESCRIPTION + " TEXT," +
									OP_DATE + " DATETIME NOT NULL," +
									OP_PHOTO + " TEXT," +
									OP_LATITUDE + " REAL," +
									OP_LONGITUDE + " REAL);";

	private static class DBHelper extends SQLiteOpenHelper {

		public DBHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_OPERATIONS_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(DBHelper.class.getName(),
							"Upgrading database from version " + oldVersion + " to "
											+ newVersion + ". Old data will be destroyed");
			db.execSQL("DROP TABLE IF EXISTS " + OPERATIONS_TABLE_NAME);
			onCreate(db);
		}
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int count;
		switch (uriMatcher.match(uri)) {
			case OPERATIONS:
				count = database.delete(OPERATIONS_TABLE_NAME, selection, selectionArgs);
				break;
			case OPERATIONS_ID:
				count = database.delete(OPERATIONS_TABLE_NAME,
								OP_ID + " = " + uri.getLastPathSegment() +
												(!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""),
								selectionArgs);
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
			case OPERATIONS:
				return "vnd.android.cursor.dir/vnd.cuentas.Operation";
			case OPERATIONS_ID:
				return "vnd.android.cursor.item/vnd.cuentas.Operation";
			default:
				throw new IllegalArgumentException("Unsupported URI " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long row = database.insert(OPERATIONS_TABLE_NAME, "", values);
		if (row > 0) {
			Uri newUri = ContentUris.withAppendedId(OPERATIONS_URI, row);
			getContext().getContentResolver().notifyChange(newUri, null);
			return newUri;
		}
		throw new SQLException("Fail to add a new record into " + uri);
	}

	@Override
	public boolean onCreate() {
		Context context = getContext();
		dbHelper = new DBHelper(context);
		database = dbHelper.getWritableDatabase();

		return database != null;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
											String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(OPERATIONS_TABLE_NAME);

		String groupBy = uri.getQueryParameter("groupBy");
		String limit = uri.getQueryParameter("limit");

		switch (uriMatcher.match(uri)) {
			case OPERATIONS:
				queryBuilder.setProjectionMap(operationsMap);
				break;
			case OPERATIONS_ID:
				queryBuilder.appendWhere(OP_ID + "=" + uri.getLastPathSegment());
				break;
			case TAGS:
				queryBuilder.setDistinct(true);
				projection = new String[]{OP_TAG,
								"count(*) AS _count",
								"min(" + OP_ID + ") AS _id"};
				groupBy = OP_TAG;
				break;
			case DAILY:
				queryBuilder.setProjectionMap(operationsMap);
				projection = new String[]{"date(" + OP_DATE + ") AS " + OP_DATE,
								"sum(" + OP_AMOUNT + ") AS " + OP_AMOUNT,
								"min(" + OP_ID + ") AS " + OP_ID};
				groupBy = "date(" + OP_DATE + ")";
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
		}

		Cursor cursor = queryBuilder.query(database, projection, selection,
						selectionArgs, groupBy, null,
						sortOrder, limit);

		// To receive update notifications when a new operation is inserted
		uri = OPERATIONS_URI;
		cursor.setNotificationUri(getContext().getContentResolver(), uri);

		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
										String[] selectionArgs) {
		int count;
		switch (uriMatcher.match(uri)) {
			case OPERATIONS:
				count = database.update(OPERATIONS_TABLE_NAME, values, selection, selectionArgs);
				break;
			case OPERATIONS_ID:
				count = database.update(OPERATIONS_TABLE_NAME, values,
								OP_ID + " = " + uri.getLastPathSegment() +
												(!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""),
								selectionArgs);
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
}
