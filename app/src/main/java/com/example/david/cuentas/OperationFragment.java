package com.example.david.cuentas;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.david.cuentas.util.CursorRecyclerViewAdapter;

import org.parceler.Parcels;

import java.text.NumberFormat;


public abstract class OperationFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	RecyclerView mOperationsList;
	DateBalanceCursorAdapter mAdapter;
	View.OnClickListener addOperationListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(OperationFragment.this.getActivity(), AddOperation.class);
			intent.putExtra(AddOperation.OPERATION_KEY, operationType());
			startActivityForResult(intent, OPERATION_REQUEST);
		}
	};

	final int OPERATIONS_LOADER = 0;
	final int OPERATION_REQUEST = 1;

	// Initialization happens in th init method, since view not exists in the constructor
	protected void init(RecyclerView operationsList) {
		mOperationsList = operationsList;
		mAdapter = new DateBalanceCursorAdapter(null); // No cursor yet
		mOperationsList.setAdapter(mAdapter);
	}

	protected abstract int operationType();

	protected abstract Operation formatOperation(Operation operation);

	protected abstract void callListener(String date);

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == OPERATION_REQUEST && resultCode == Activity.RESULT_OK) {

			Operation operation = Parcels.unwrap(data.getParcelableExtra(AddOperation.RESULT_KEY));
			operation = formatOperation(operation);

			ContentValues values = new ContentValues();
			values.put(OperationsProvider.OP_DESCRIPTION, operation.getDescription());
			values.put(OperationsProvider.OP_AMOUNT, operation.getAmount());
			values.put(OperationsProvider.OP_TAG, operation.getTag());
			values.put(OperationsProvider.OP_DATE, operation.getFormattedDate());
			values.put(OperationsProvider.OP_PHOTO, operation.getPhoto());
			values.put(OperationsProvider.OP_LATITUDE, operation.getLatitude());
			values.put(OperationsProvider.OP_LONGITUDE, operation.getLongitude());

			getActivity().getContentResolver().insert(OperationsProvider.OPERATIONS_URI, values);
			getLoaderManager().initLoader(OPERATIONS_LOADER, null, this);
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		switch (loader.getId()) {
			case (OPERATIONS_LOADER):
				mAdapter.changeCursor(data);
				break;
			default:
				throw new IllegalArgumentException("Unknown loader id: " + loader.getId());
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		onLoadFinished(loader, null);
	}

	public class DateBalanceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		protected TextView mDate;
		protected TextView mAmount;

		public DateBalanceViewHolder(View v) {
			super(v);
			mDate = (TextView) v.findViewById(R.id.date_group);
			mAmount = (TextView) v.findViewById(R.id.date_balance);
		}

		@Override
		public void onClick(View v) {
			callListener(mDate.getText().toString());
		}
	}

	public class DateBalanceCursorAdapter extends CursorRecyclerViewAdapter<DateBalanceViewHolder> {

		public DateBalanceCursorAdapter(Cursor cursor) {
			super(cursor);
		}

		@Override
		public void onBindViewHolderCursor(DateBalanceViewHolder holder, Cursor cursor) {
			holder.mDate.setText(cursor.getString(cursor.getColumnIndex(OperationsProvider.OP_DATE)));

			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			holder.mAmount.setText(formatter.format(cursor.getDouble(cursor.getColumnIndex(OperationsProvider.OP_AMOUNT))));
		}

		@Override
		public DateBalanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View dateBalanceView = LayoutInflater
							.from(parent.getContext())
							.inflate(R.layout.date_view, parent, false);
			DateBalanceViewHolder holder = new DateBalanceViewHolder(dateBalanceView);
			dateBalanceView.setOnClickListener(holder);
			return holder;
		}
	}
}
