package com.example.david.cuentas;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

public class ChartFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
	private PieChart mChart;

	private static final String EXPENSES_COL = "expenses";
	final int TAGS_LOADER = 0;

	public static ChartFragment newInstance() {
		return new ChartFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_charts, container, false);
		mChart = (PieChart) view.findViewById(R.id.chart);
		mChart.setUsePercentValues(true);
		mChart.setHoleColorTransparent(true);

		//tf = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Regular.ttf");

		//mChart.setCenterTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Light.ttf"));

		mChart.setHoleRadius(60f);

		mChart.setDescription("");

		mChart.setDrawCenterText(true);

		mChart.setDrawHoleEnabled(true);

		mChart.setRotationEnabled(false);
		mChart.setCenterText("Expenses");

		mChart.animateXY(1500, 1500);
		// mChart.spin(2000, 0, 360);

		getLoaderManager().initLoader(TAGS_LOADER, null, this);
		return view;
	}

	private void setData(Cursor data) {
		ArrayList<Entry> yVals = new ArrayList<>();
		ArrayList<String> xVals = new ArrayList<>();
		int xIndex = 0;
		data.moveToFirst();
		if (data.getCount() > 0) {
			do {
				yVals.add(new Entry(data.getFloat(data.getColumnIndex(EXPENSES_COL)), xIndex));
				xVals.add(data.getString(data.getColumnIndex(OperationsProvider.OP_TAG)));
				xIndex++;
			} while (data.moveToNext());
		}

		PieDataSet dataSet = new PieDataSet(yVals, "Expenses by tag");
		dataSet.setSliceSpace(3f);
		dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

		PieData pieData = new PieData(xVals, dataSet);
		pieData.setValueFormatter(new PercentFormatter());
		pieData.setValueTextColor(Color.WHITE);
		pieData.setValueTextSize(11f);

		mChart.setData(pieData);
		mChart.highlightValues(null);
		mChart.invalidate();

		// Set the chart legend, this must be done after setting the data
		Legend l = mChart.getLegend();
		l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
		l.setXEntrySpace(7f);
		l.setYEntrySpace(5f);

	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id) {
			case TAGS_LOADER:
				Uri queryUri = OperationsProvider.OPERATIONS_URI.buildUpon()
								.appendQueryParameter("groupBy", OperationsProvider.OP_TAG).build();
				final String amount = OperationsProvider.OP_AMOUNT;
				return new CursorLoader(getActivity(), queryUri,
								new String[]{"min(" + OperationsProvider.OP_ID + ") AS " + OperationsProvider.OP_ID,
												OperationsProvider.OP_TAG,
												"sum(case when " + amount + " < 0 THEN " + amount + " ELSE 0 END) AS " + EXPENSES_COL},
								OperationsProvider.OP_AMOUNT + " < 0",
								null,
								null);
			default:
				throw new IllegalArgumentException("Unknown loader id: " + id);
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		setData(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

	}
}
