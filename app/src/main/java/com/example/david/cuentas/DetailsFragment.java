package com.example.david.cuentas;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.david.cuentas.util.CursorRecyclerViewAdapter;

import java.text.NumberFormat;

public class DetailsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	public static DetailsFragment newInstance(String date, int opetationType) {
		Bundle args = new Bundle();
		args.putString(DATE_KEY, date);
		args.putInt(OPERATION_TYPE_KEY, opetationType);
		DetailsFragment instance = new DetailsFragment();
		instance.setArguments(args);
		return instance;
	}

	public static DetailsFragment newInstance(String tag) {
		Bundle args = new Bundle();
		args.putString(TAG_KEY, tag);
		DetailsFragment instance = new DetailsFragment();
		instance.setArguments(args);
		return instance;
	}

	public static final String TAG_KEY = "TAG";
	public static final String DATE_KEY = "DATE";
	public static final String OPERATION_TYPE_KEY = "OPERATION_TYPE";

	final int DETAILS_DATE_LOADER = 0;
	final int DETAILS_TAG_LOADER = 1;

	public static final int OPERATION_EXPENSE = 0;
	public static final int OPERATION_INCOME = 1;

	private String tagParameter;
	private String dateParameter;
	private int operationTypeParameter;

	private OperationCursorAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_details, container, false);
		RecyclerView mDetailsList = (RecyclerView) view.findViewById(R.id.recycler_view);


		mDetailsList.setHasFixedSize(true);
		LinearLayoutManager manager = new LinearLayoutManager(getActivity());
		manager.setOrientation(LinearLayoutManager.VERTICAL);
		mDetailsList.setLayoutManager(manager);

		mAdapter = new OperationCursorAdapter(null);
		mDetailsList.setAdapter(mAdapter);

		Bundle args = getArguments();
		tagParameter = args.getString(TAG_KEY);
		dateParameter = args.getString(DATE_KEY);
		operationTypeParameter = args.getInt(OPERATION_TYPE_KEY);

		if (tagParameter != null) {
			getLoaderManager().initLoader(DETAILS_TAG_LOADER, null, this);
		} else if (dateParameter != null) {
			getLoaderManager().initLoader(DETAILS_DATE_LOADER, null, this);
		}
		return view;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id) {
			case DETAILS_DATE_LOADER:
				String where;
				if (operationTypeParameter == OPERATION_EXPENSE) {
					where = OperationsProvider.OP_AMOUNT + " < 0 AND ";
				} else {
					where = OperationsProvider.OP_AMOUNT + " > 0 AND ";
				}
				return new CursorLoader(getActivity(),
								OperationsProvider.OPERATIONS_URI,
								new String[]{OperationsProvider.OP_ID,
												OperationsProvider.OP_DESCRIPTION,
												OperationsProvider.OP_TAG,
												"date(" + OperationsProvider.OP_DATE + ") AS " + OperationsProvider.OP_DATE,
												OperationsProvider.OP_AMOUNT,
												OperationsProvider.OP_PHOTO,
												OperationsProvider.OP_LATITUDE,
												OperationsProvider.OP_LONGITUDE},
								where + OperationsProvider.OP_DATE
												+ " BETWEEN datetime('" + dateParameter + " 00:00:00') "
												+ "AND datetime('" + dateParameter + " 23:59:59')",
								null,
								null);
			case DETAILS_TAG_LOADER:
				return new CursorLoader(getActivity(),
								OperationsProvider.OPERATIONS_URI,
								new String[]{OperationsProvider.OP_ID,
												OperationsProvider.OP_DESCRIPTION,
												OperationsProvider.OP_TAG,
												"date(" + OperationsProvider.OP_DATE + ") AS " + OperationsProvider.OP_DATE,
												OperationsProvider.OP_AMOUNT,
												OperationsProvider.OP_PHOTO,
												OperationsProvider.OP_LATITUDE,
												OperationsProvider.OP_LONGITUDE},
								OperationsProvider.OP_TAG + " = '" + tagParameter + "'",
								null,
								null);
			default:
				throw new IllegalArgumentException("Unknown loader id: " + id);
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		switch (loader.getId()) {
			case DETAILS_TAG_LOADER:
			case DETAILS_DATE_LOADER:
				mAdapter.changeCursor(data);
				break;
			default:
				throw new IllegalArgumentException("Unknown loader id: " + loader.getId());
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		onLoadFinished(loader, null);
	}

	public static class OperationViewHolder extends RecyclerView.ViewHolder {
		protected TextView mName;
		protected TextView mAmount;
		protected TextView mTag;
		protected ImageView mPhoto;
		protected Button mLocation;

		public OperationViewHolder(View v) {
			super(v);
			mName = (TextView) v.findViewById(R.id.op_name);
			mAmount = (TextView) v.findViewById(R.id.op_amount);
			mTag = (TextView) v.findViewById(R.id.op_tag);
			mPhoto = (ImageView) v.findViewById(R.id.op_photo);
			mLocation = (Button) v.findViewById(R.id.op_location);
		}
	}

	public class OperationCursorAdapter extends CursorRecyclerViewAdapter<OperationViewHolder> {

		public OperationCursorAdapter(Cursor cursor) {
			super(cursor);
		}

		@Override
		public void onBindViewHolderCursor(OperationViewHolder holder, Cursor cursor) {
			holder.mName.setText(cursor.getString(cursor.getColumnIndex(OperationsProvider.OP_DESCRIPTION)));
			holder.mTag.setText(cursor.getString(cursor.getColumnIndex(OperationsProvider.OP_TAG)));

			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			holder.mAmount.setText(formatter.format(cursor.getDouble(cursor.getColumnIndex(OperationsProvider.OP_AMOUNT))));

			String photoFileName = cursor.getString(cursor.getColumnIndex(OperationsProvider.OP_PHOTO));

			// Load the thumbnail
			Bitmap image = BitmapFactory.decodeFile(photoFileName + AddOperation.THUMBNAIL_SUFIX + ".jpg");
			if (image != null) {
				holder.mPhoto.setImageBitmap(image);
			}

			// Save the file path as a tag
			holder.mPhoto.setTag(photoFileName);

			Double latitude = cursor.getDouble(cursor.getColumnIndex(OperationsProvider.OP_LATITUDE));
			Double longitude = cursor.getDouble(cursor.getColumnIndex(OperationsProvider.OP_LONGITUDE));

			if (latitude != null && longitude != null) {
				// Save the location URI as tag
				holder.mLocation.setTag(Uri.parse("geo:" + latitude + "," + longitude));
				holder.mLocation.setVisibility(View.VISIBLE);
			} else {
				holder.mLocation.setVisibility(View.INVISIBLE);
			}
		}

		@Override
		public OperationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View operationView = LayoutInflater
							.from(parent.getContext())
							.inflate(R.layout.operation_view, parent, false);
			OperationViewHolder holder = new OperationViewHolder(operationView);
			//Show big picture onClick()
			holder.mPhoto.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String photoFile = (String) v.getTag();
					Intent intent = new Intent();
					intent.setAction(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.parse("file://" + photoFile + ".jpg"), "image/*");
					startActivity(intent);
				}
			});

			holder.mLocation.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Intent.ACTION_VIEW, (Uri) v.getTag());
					intent.setPackage("com.google.android.apps.maps");
					startActivity(intent);
				}
			});
			return holder;
		}
	}
}
