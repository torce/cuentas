package com.example.david.cuentas;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.david.cuentas.view.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity implements
				ExpensesFragment.ExpenseClickListener,
				IncomesFragment.IncomeClickListener,
				TagGroupFragment.TagGroupClickListener {

	private FrameLayout mDetailContainer;
	private TextView mTextInfo;

	@Override
	public void onExpenseClick(String date) {
		if (mDetailContainer == null) {
			Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
			intent.putExtra(DetailsFragment.DATE_KEY, date);
			intent.putExtra(DetailsFragment.OPERATION_TYPE_KEY, DetailsFragment.OPERATION_EXPENSE);
			startActivity(intent);
		} else {
			replaceDetailsFragment(DetailsFragment.newInstance(date, DetailsFragment.OPERATION_EXPENSE));
		}
	}

	@Override
	public void onIncomeClick(String date) {
		if (mDetailContainer == null) {
			Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
			intent.putExtra(DetailsFragment.DATE_KEY, date);
			intent.putExtra(DetailsFragment.OPERATION_TYPE_KEY, DetailsFragment.OPERATION_INCOME);
			startActivity(intent);
		} else {
			replaceDetailsFragment(DetailsFragment.newInstance(date, DetailsFragment.OPERATION_INCOME));
		}
	}

	@Override
	public void onTagGroupClick(String tag) {
		if (mDetailContainer == null) {
			Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
			intent.putExtra(DetailsFragment.TAG_KEY, tag);
			startActivity(intent);
		} else {
			replaceDetailsFragment(DetailsFragment.newInstance(tag));
		}
	}

	private void replaceDetailsFragment(Fragment fragment) {
		getFragmentManager().beginTransaction().replace(mDetailContainer.getId(), fragment).commit();
		mTextInfo.setVisibility(View.INVISIBLE);
	}


	/**
	 * This class represents a tab to be displayed by {@link android.support.v4.view.ViewPager} and it's associated
	 * {@link com.example.david.cuentas.view.SlidingTabLayout}.
	 */
	static class SamplePagerItem {
		private final CharSequence mTitle;
		private final int mIndicatorColor;
		private final int mDividerColor;
		private final Fragment mFragment;

		SamplePagerItem(CharSequence title, int indicatorColor, int dividerColor, Fragment fragment) {
			mTitle = title;
			mIndicatorColor = indicatorColor;
			mDividerColor = dividerColor;
			mFragment = fragment;
		}

		/**
		 * @return A new {@link android.support.v4.app.Fragment} to be displayed by a {@link android.support.v4.view.ViewPager}
		 */
		Fragment getFragment() {
			return mFragment;
		}

		/**
		 * @return the title which represents this tab. In this sample this is used directly by
		 * {@link android.support.v4.view.PagerAdapter#getPageTitle(int)}
		 */
		CharSequence getTitle() {
			return mTitle;
		}

		/**
		 * @return the color to be used for indicator on the {@link com.example.david.cuentas.view.SlidingTabLayout}
		 */
		int getIndicatorColor() {
			return mIndicatorColor;
		}

		/**
		 * @return the color to be used for right divider on the {@link com.example.david.cuentas.view.SlidingTabLayout}
		 */
		int getDividerColor() {
			return mDividerColor;
		}
	}

	/**
	 * The {@link android.support.v13.app.FragmentPagerAdapter} used to display pages in this sample. The individual pages
	 * are instances of {@link android.app.Fragment} which just display three lines of text. Each page is
	 * created by the relevant {@link SamplePagerItem} for the requested position.
	 * <p/>
	 * The important section of this class is the {@link #getPageTitle(int)} method which controls
	 * what is displayed in the {@link com.example.david.cuentas.view.SlidingTabLayout}.
	 */
	class SampleFragmentPagerAdapter extends FragmentPagerAdapter {

		SampleFragmentPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		/**
		 * Return the {@link android.support.v4.app.Fragment} to be displayed at {@code position}.
		 * <p/>
		 */
		@Override
		public Fragment getItem(int i) {
			return mTabs.get(i).getFragment();
		}

		@Override
		public int getCount() {
			return mTabs.size();
		}

		// BEGIN_INCLUDE (pageradapter_getpagetitle)

		/**
		 * Return the title of the item at {@code position}. This is important as what this method
		 * returns is what is displayed in the {@link com.example.david.cuentas.view.SlidingTabLayout}.
		 * <p/>
		 * Here we return the value returned from {@link SamplePagerItem#getTitle()}.
		 */
		@Override
		public CharSequence getPageTitle(int position) {
			return mTabs.get(position).getTitle();
		}
		// END_INCLUDE (pageradapter_getpagetitle)

	}

	/**
	 * A custom {@link android.support.v4.view.ViewPager} title strip which looks much like Tabs present in Android v4.0 and
	 * above, but is designed to give continuous feedback to the user when scrolling.
	 */
	private SlidingTabLayout mSlidingTabLayout;

	/**
	 * A {@link android.support.v4.view.ViewPager} which will be used in conjunction with the {@link SlidingTabLayout} above.
	 */
	private ViewPager mViewPager;

	/**
	 * List of {@link SamplePagerItem} which represent this sample's tabs.
	 */
	private List<SamplePagerItem> mTabs = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sliding_tabs_main);

		// BEGIN_INCLUDE (populate_tabs)
		/**
		 * Populate our tab list with tabs. Each item contains a title, indicator color and divider
		 * color, which are used by {@link SlidingTabLayout}.
		 */
		mDetailContainer = (FrameLayout) findViewById(R.id.detail_fragment);
		mTextInfo = (TextView) findViewById(R.id.text_info);

		mTabs.add(new SamplePagerItem(
						getString(R.string.expenses_tab), // Title
						Color.RED, // Indicator color
						Color.GRAY, // Divider color
						ExpensesFragment.newInstance()
		));

		mTabs.add(new SamplePagerItem(
						getString(R.string.incomes_tab), // Title
						Color.GREEN, // Indicator color
						Color.GRAY, // Divider color
						IncomesFragment.newInstance()
		));

		mTabs.add(new SamplePagerItem(
						getString(R.string.tags_tab), // Title
						Color.BLUE, // Indicator color
						Color.GRAY, // Divider color
						TagGroupFragment.newInstance()
		));

		mTabs.add(new SamplePagerItem(
						getString(R.string.charts_tab),
						Color.YELLOW,
						Color.GRAY,
						ChartFragment.newInstance()
		));

		// END_INCLUDE (populate_tabs)

		// BEGIN_INCLUDE (setup_viewpager)
		// Get the ViewPager and set it's PagerAdapter so that it can display items
		mViewPager = (ViewPager) findViewById(R.id.viewpager);
		mViewPager.setAdapter(new SampleFragmentPagerAdapter(getFragmentManager()));
		// END_INCLUDE (setup_viewpager)

		// BEGIN_INCLUDE (setup_slidingtablayout)
		// Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
		// it's PagerAdapter set.
		mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
		mSlidingTabLayout.setViewPager(mViewPager);

		// BEGIN_INCLUDE (tab_colorizer)
		// Set a TabColorizer to customize the indicator and divider colors. Here we just retrieve
		// the tab at the position, and return it's set color
		mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

			@Override
			public int getIndicatorColor(int position) {
				return mTabs.get(position).getIndicatorColor();
			}

			@Override
			public int getDividerColor(int position) {
				return mTabs.get(position).getDividerColor();
			}

		});
		// END_INCLUDE (tab_colorizer)
		// END_INCLUDE (setup_slidingtablayout)
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
}
