package com.example.david.cuentas;

import org.parceler.Parcel;

import java.text.SimpleDateFormat;
import java.util.Date;

@Parcel
public class Operation {
	double amount;
	String tag;
	String description;
	Date date;
	String photo;
	Double latitude;
	Double longitude;

	/**
	 * Empty constructor required by {@link org.parceler} *
	 */
	public Operation() {
	}


	public Operation(double amount, String tag, String description, Date date, String photo, Double latitude, Double longitude) {
		this.amount = amount;
		this.tag = tag;
		this.description = description;
		this.date = date;
		this.photo = photo;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getDescription() {
		return description;
	}

	public String getTag() {
		return tag;
	}

	public double getAmount() {
		return amount;
	}

	public Date getDate() {
		return date;
	}

	public String getFormattedDate() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

	public String getPhoto() {
		return photo;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
}
