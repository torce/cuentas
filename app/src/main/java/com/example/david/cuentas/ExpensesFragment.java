package com.example.david.cuentas;

import android.app.Activity;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.melnykov.fab.FloatingActionButton;

public class ExpensesFragment extends OperationFragment {

	public interface ExpenseClickListener {
		public void onExpenseClick(String date);
	}

	private ExpenseClickListener mListener;

	public static ExpensesFragment newInstance() {
		return new ExpensesFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
													 Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_operations_list, container, false);

		RecyclerView mExpenseList = (RecyclerView) view.findViewById(R.id.recycler_view);
		mExpenseList.setHasFixedSize(true);
		LinearLayoutManager manager = new LinearLayoutManager(getActivity());
		manager.setOrientation(LinearLayoutManager.VERTICAL);
		mExpenseList.setLayoutManager(manager);

		FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
		fab.attachToRecyclerView(mExpenseList);
		fab.setOnClickListener(addOperationListener);

		getLoaderManager().initLoader(OPERATIONS_LOADER, null, this);

		init(mExpenseList);

		return view;
	}

	@Override
	protected int operationType() {
		return AddOperation.OPERATION_EXPENSE;
	}

	@Override
	protected Operation formatOperation(Operation operation) {
		operation.setAmount(-operation.getAmount());
		return operation;
	}

	@Override
	protected void callListener(String date) {
		mListener.onExpenseClick(date);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id) {
			case OPERATIONS_LOADER:
				return new CursorLoader(getActivity(),
								OperationsProvider.DAILY_URI,
								new String[]{OperationsProvider.OP_ID,
												OperationsProvider.OP_DATE,
												OperationsProvider.OP_AMOUNT},
								OperationsProvider.OP_AMOUNT + " <= 0",
								null,
								OperationsProvider.OP_DATE + " DESC");
			default:
				throw new IllegalArgumentException("Unknown loader id: " + id);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mListener = (ExpenseClickListener) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}
}
